#ifndef AST_H
#define AST_H

#include <string>
#include <vector>
#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"

using namespace llvm;

class ASTPrototype;

static llvm::LLVMContext TheContext;
static llvm::IRBuilder<> Builder(TheContext);
static std::unique_ptr<llvm::Module> TheModule = 
			llvm::make_unique<llvm::Module>("my cool jit", TheContext);
static std::map<std::string, llvm::AllocaInst *> NamedValues;
static std::map<std::string, ASTPrototype *> FunctionProtos;

class ASTExpr
{
public:
	virtual ~ASTExpr() = default;
	virtual llvm::Value * CodeGen() = 0;
	virtual void Print() = 0;
};

class ASTNumber : public ASTExpr
{
public:
	ASTNumber(double value);
	llvm::Value * CodeGen() override;
	void Print() override;
	double Value;
};

class ASTVar : public ASTExpr
{
public:
	ASTVar(std::string name);
	llvm::Value * CodeGen() override;
	void Print() override;
	std::string Name;
};

enum BinOpType
{
	opPLUS, opMINUS, opTIMES, opDIVIDE, opEQ, opNEQ, opGT,
	opLT, opGTE, opLTE, opMOD, opASSIGN
};

std::string ToString(BinOpType opType);

static std::map<BinOpType, int> BinOpPrecedenceTable
{ 
	{ opPLUS, 30 },
	{ opMINUS, 30 },
	{ opTIMES, 40 },
	{ opDIVIDE, 40 },
	{ opMOD, 40 },
	{ opEQ, 10 },
	{ opNEQ, 10 },
	{ opLT, 10 },
	{ opGT, 10 },
	{ opGTE, 10 },
	{ opASSIGN, 2 },
	{ opLTE, 10 }
};

class ASTBinOp : public ASTExpr
{
public:
	ASTBinOp(BinOpType opType, ASTExpr * lhs, ASTExpr * rhs);
	llvm::Value * CodeGen() override;
	void Print() override;
	BinOpType OpType;
	ASTExpr * Lhs; 
	ASTExpr * Rhs;
};

class ASTCall : public ASTExpr
{
public:
	ASTCall(std::string callee, std::vector<ASTExpr *> args);
	llvm::Value * CodeGen() override;
	void Print() override;
	std::string Callee;
	std::vector<ASTExpr *> Args;
};

class ASTIf : public ASTExpr
{
public:
	ASTIf(ASTExpr * cond, ASTExpr * then, ASTExpr * els);
	llvm::Value * CodeGen() override;
	void Print() override;
	ASTExpr * Cond;
	ASTExpr * Then;
	ASTExpr * Else;
};

/* Function name + function parameters */
class ASTPrototype
{
public:
	ASTPrototype(std::string name, std::vector<std::string> params);
	llvm::Function * CodeGen();
	void Print();
	std::string Name;
	std::vector<std::string> Params;
};

class ASTFunction
{
public:
	ASTFunction(ASTPrototype * proto, ASTExpr * body, std::vector<ASTVar *> vars);
	llvm::Function * CodeGen();
	void Print();
	ASTPrototype * Proto;
	ASTExpr * Body;
	std::vector<ASTVar *> Vars;
};

class ASTProgram
{
public:
	ASTProgram(std::vector<ASTFunction *> glFuncs, std::vector<ASTVar *> glVars);
	void CodeGen();
	void Print();
	std::vector<ASTFunction *> GlFuncs;
	std::vector<ASTVar *> GlVars;
};

#endif /* AST_H */