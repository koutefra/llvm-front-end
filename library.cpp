#include "library.hpp"

extern "C" DLLEXPORT double readvar()
{
	double a;
	scanf("%lf",&a);
	return a;
}

extern "C" DLLEXPORT double printvar(double x)
{
	fprintf(stderr, "%f\n", x);
	return 0;
}

extern "C" DLLEXPORT double getrand(double from, double to)
{
	srand(time(NULL));
	return (double)(rand() % (long)to) + from;
}

extern "C" DLLEXPORT double sleepmil(double milis)
{
	return (double)usleep((long)milis);
}