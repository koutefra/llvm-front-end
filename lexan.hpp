#ifndef LEXAN_H
#define LEXAN_H

#include <map>
#include <string>

enum LexSymbol
{
	IDENT, NUMBER,
	kwDEF, kwVAR, kwIF, kwTHEN, kwENDIF, kwEXT, kwELSE, // keywords
	PLUS, MINUS, TIMES, DIVIDE, EQ, NEQ, GT, GTE, LT, LTE, MOD, ASSIGN, // operators
	COMMA, LPAR, RPAR,
	EOI, // end of input
	ERR
};

std::string ToString(LexSymbol symbol);

static std::map<std::string, LexSymbol> KeywordTable
{ 
	{ "def", kwDEF },
	{ "then", kwTHEN },
	{ "endif", kwENDIF },
	{ "var", kwVAR },
	{ "extern", kwEXT },
	{ "else", kwELSE },
	{ "if", kwIF }
};

struct Token
{
	Token() = default;
	Token(LexSymbol symbol, std::string value);
	LexSymbol Symbol;
	std::string Value;
	bool IsOperator() const;
};

class Lexan
{
public:
	/* Loads next token. */
	Lexan();
	Token GetLast() const;
	Token GetNextWithoutShift() const;
	/* Returns next token and loads new next token. If token isn't valid, throws error. */
	Token GetNext();
	/* Returns true if argument token matches with next token, otherwise returns false. */
	bool IsNext(LexSymbol symbol) const;
	/* Returns true if argument token type matches with one of the operators, 
	otherwise returns false. */
	bool IsNextOperator() const;
	/* Checks if argument token matches with next token. 
	If so, loads new next token, otherwise throws an error. */
	Token Match(LexSymbol symbol);
private:
	Token nextToken;
	Token lastToken;
	char nextChar;
	int lineNumber;
	/* Parses input to token and returns. If token isn't valid, throws error. */
	Token getNextToken();
	LexSymbol findKeyword(std::string value) const;
	/* Tryes to parse input to operator. Returns LexSymbol::ERR if there is not an operator. 
	It loads new next char. */
	LexSymbol parseOperator();
};

#endif /* LEXAN_H */