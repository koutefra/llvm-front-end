#ifndef PARSER_H
#define PARSER_H

#include <list>
#include "lexan.hpp"
#include "ast.hpp"

class Parser
{
public:
	void CodeGen();
private:
	void handleFuncDef();
	void handleExt();
	ASTFunction * parseFunction();
	ASTPrototype * parsePrototype();
	std::vector<std::string> parseParams();
	std::vector<ASTExpr *> parseArgs();
	ASTExpr * parseExpr();
	ASTIf * parseIf();
	BinOpType getBinOpType(LexSymbol symbol) const;
	ASTExpr * parseBinOpRHS(int exprPrec, ASTExpr * lhs);
	ASTExpr * parsePrimary();
	Lexan lexan;
	ASTProgram * program;
	std::vector<ASTVar *> curFuncVars;
	void registerVar(ASTVar * var);
};

#endif /* PARSER_H */