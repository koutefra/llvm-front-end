#!/bin/bash
for FPATH in ./Samples/*.p; do
	FNAME=$(basename "$FPATH" .p)
	./compiler < "$FPATH" &> "./Samples/$FNAME.ll"; clang "./Samples/$FNAME.ll" ./library.o -o "./Samples/$FNAME.out"
done
