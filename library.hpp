#ifndef LIBRARY_H
#define LIBRARY_H

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

// "Library" functions that can be "extern'd" from user code.
#ifdef _WIN32
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT
#endif

extern "C" DLLEXPORT double readvar();
/// printd - printf that takes a double prints it as "%f\n", returning 0.
extern "C" DLLEXPORT double printvar(double x);
extern "C" DLLEXPORT double getrand(double from, double to);
extern "C" DLLEXPORT double sleepmil(double duration);

#endif /* LIBRARY_H */