#include "parser.hpp"

void Parser::CodeGen()
{
	while (!lexan.IsNext(EOI))
	{
		if (lexan.IsNext(kwDEF))
		{
			handleFuncDef();
		}
		else if (lexan.IsNext(kwEXT))
		{
			handleExt();
		}
		else
		{
			throw "Parser error\n"; // TO DO
		}
	}
}

void Parser::handleFuncDef()
{
	if (auto func = parseFunction()) 
	{
		if (auto * funcIR = func->CodeGen()) 
		{
			funcIR->print(errs());
			fprintf(stderr, "\n");
		}
	}
}

void Parser::handleExt()
{
	lexan.Match(kwEXT);
	if (auto proto = parsePrototype()) 
	{
		if (auto * protoIR = proto->CodeGen()) 
		{
			protoIR->print(errs());
			fprintf(stderr, "\n");
			FunctionProtos[proto->Name] = std::move(proto);
		}
	}
}

ASTFunction * Parser::parseFunction() 
{
	curFuncVars.clear();

	lexan.Match(kwDEF);
	ASTPrototype * proto = parsePrototype();
	ASTExpr * body = parseExpr();

	// delete paramater variables from function declared variables
	for (int i = 0; i < proto->Params.size(); i++)
	{
		for (int j = 0; j < curFuncVars.size(); j++)
		{
			if (proto->Params[i] == curFuncVars[j]->Name)
			{
				curFuncVars.erase(curFuncVars.begin() + j);
				break;
			}
		}
	}
	
	return new ASTFunction(proto, body, curFuncVars);
}

ASTPrototype * Parser::parsePrototype()
{
	Token token = lexan.Match(IDENT);
	std::string name = token.Value;
	lexan.Match(LPAR);

	ASTPrototype * prototype = new ASTPrototype(name, parseParams());
	lexan.Match(RPAR);
	return prototype;
}

std::vector<std::string> Parser::parseParams()
{
	std::vector<std::string> result;

	Token token;
	while (lexan.IsNext(IDENT))
	{
		token = lexan.Match(IDENT);
		result.push_back(token.Value);
		if (lexan.IsNext(COMMA)) // skip the comma
			lexan.GetNext();
	}

	return result;
}

std::vector<ASTExpr *> Parser::parseArgs()
{
	std::vector<ASTExpr *> result;

	Token token;
	while (!lexan.IsNext(RPAR))
	{
		result.push_back(parseExpr());
		if (!lexan.IsNext(COMMA))
			break;
		lexan.GetNext(); // skip comma
	}

	return result;	
}

ASTIf * Parser::parseIf()
{
	lexan.Match(kwIF);
	lexan.Match(LPAR);
	ASTExpr * cond = parseExpr();
	lexan.Match(RPAR);
	lexan.Match(kwTHEN);
	ASTExpr * then = parseExpr();
	lexan.Match(kwELSE);
	ASTExpr * els = parseExpr();
	lexan.Match(kwENDIF);
	return new ASTIf(cond, then, els);
}

ASTExpr * Parser::parseExpr() 
{
	ASTExpr * lhs = parsePrimary();
	return parseBinOpRHS(0, lhs);
}

ASTExpr * Parser::parseBinOpRHS(int exprPrec, ASTExpr * lhs)
{
	while (true) 
	{
		if (!lexan.IsNextOperator())
			return lhs;
		Token token = lexan.GetNextWithoutShift();
		BinOpType lhsOpType = getBinOpType(token.Symbol);
		int lhsPrec = BinOpPrecedenceTable[lhsOpType];

		if (lhsPrec < exprPrec)
			return lhs;

		lexan.GetNext(); // eat operator
		ASTExpr * rhs = parsePrimary();

		if (lexan.IsNextOperator())
		{
			token = lexan.GetNextWithoutShift();
			BinOpType rhsOpType = getBinOpType(token.Symbol);
			int rhsPrec = BinOpPrecedenceTable[rhsOpType];
			if (lhsPrec < rhsPrec)
				rhs = parseBinOpRHS(lhsPrec + 1, rhs);
		}

		lhs = new ASTBinOp(lhsOpType, lhs, rhs);
	}	
}

ASTExpr * Parser::parsePrimary()
{
	ASTExpr * expr;
	if (lexan.IsNext(LPAR))
	{
		lexan.Match(LPAR);
		expr = parseExpr();
		lexan.Match(RPAR);
	}
	else if (lexan.IsNext(IDENT)) // variable or function call
	{
		std::string name = lexan.Match(IDENT).Value;
		if (lexan.IsNext(LPAR)) // function call
		{
			lexan.Match(LPAR);
			expr = new ASTCall(name, parseArgs());
			lexan.Match(RPAR);
		}
		else // variable
		{
			ASTVar * var = new ASTVar(name);
			registerVar(var);
			expr = var;
		}
	}
	else if (lexan.IsNext(NUMBER))
	{
		double value = std::stod(lexan.Match(NUMBER).Value);
		expr = new ASTNumber(value);
	}
	else if (lexan.IsNext(MINUS)) // unary minus
	{
		lexan.Match(MINUS);
		expr = new ASTBinOp(opMINUS, parseExpr(), NULL);
	}
	else if (lexan.IsNext(kwIF))
	{
		expr = parseIf();
	}
	else
	{
		throw "Parse expr error 2\n";
	}

	return expr;
}

BinOpType Parser::getBinOpType(LexSymbol symbol) const
{
	switch (symbol)
	{
		case PLUS:
			return opPLUS;
		case MINUS:
			return opMINUS;
		case TIMES:
			return opTIMES;
		case DIVIDE:
			return opDIVIDE;
		case EQ:
			return opEQ;
		case NEQ:
			return opNEQ;
		case GT:
			return opGT;
		case GTE:
			return opGTE;
		case LT:
			return opLT;
		case LTE:
			return opLTE;
		case MOD:
			return opMOD;
		case ASSIGN:
			return opASSIGN;
		default:
			throw "Invalid binary operator";
	}
}

void Parser::registerVar(ASTVar * var)
{
	for (int i = 0; i < curFuncVars.size(); i++)
	{
		if (var->Name == curFuncVars[i]->Name)
			return;
	}

	curFuncVars.push_back(var);
}