
declare double @print(double)


declare double @read()


define i32 @main() {
entry:
  %x1 = alloca double
  %x = alloca double
  %calltmp = call double @read()
  store double %calltmp, double* %x1
  %cmptmp = fcmp ueq double %calltmp, 5.000000e+00
  %booltmp = uitofp i1 %cmptmp to double
  %ifcond = fcmp one double %booltmp, 0.000000e+00
  br i1 %ifcond, label %then, label %else

then:                                             ; preds = %entry
  %x2 = load double, double* %x1
  %calltmp3 = call double @print(double %x2)
  br label %ifcont

else:                                             ; preds = %entry
  %calltmp4 = call double @print(double 0.000000e+00)
  br label %ifcont

ifcont:                                           ; preds = %else, %then
  %iftmp = phi double [ %calltmp3, %then ], [ %calltmp4, %else ]
  %inttmp = fptosi double %iftmp to i32
  ret i32 %inttmp
}

