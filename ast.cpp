#include "ast.hpp"

Value * LogErrorV(std::string message) 
{
	printf("%s\n", message.c_str());
	// LogError(message); // print error message somewhere !!!
	return NULL;
}

Function * GetFunction(std::string Name) 
{
	// First, see if the function has already been added to the current module.
	if (auto * f = TheModule->getFunction(Name))
		return f;

	// If not, check whether we can codegen the declaration from some existing
	// prototype.
	auto fi = FunctionProtos.find(Name);
	if (fi != FunctionProtos.end())
		return fi->second->CodeGen();

	// If no existing prototype exists, return null.
	return NULL;
}

/// CreateEntryBlockAlloca - Create an alloca instruction in the entry block of
/// the function.  This is used for mutable variables etc.
static AllocaInst *CreateEntryBlockAlloca(Function * theFunction,
	const std::string & varName) 
{
	IRBuilder<> tmpB(&theFunction->getEntryBlock(),
				 theFunction->getEntryBlock().begin());
	return tmpB.CreateAlloca(Type::getDoubleTy(TheContext), NULL, varName);
}

ASTNumber::ASTNumber(double value)
{
	Value = value;
}

Value * ASTNumber::CodeGen()
{
	return ConstantFP::get(TheContext, APFloat(Value));
}

ASTVar::ASTVar(std::string name)
{
	Name = name;
}

Value * ASTVar::CodeGen()
{
	// Look this variable up in the function.
	Value * val = NamedValues[Name];
	if (!val)
		return LogErrorV("Unknown variable name");

	// Load the value.
	return Builder.CreateLoad(val, Name.c_str());
}

ASTBinOp::ASTBinOp(BinOpType opType, ASTExpr * lhs, ASTExpr * rhs)
{
	OpType = opType;
	Lhs = lhs;
	Rhs = rhs;
}

Value * ASTBinOp::CodeGen()
{
	if (!Rhs) // Unary
	{
		if (OpType != opMINUS)
			throw "Unsupported operation";

		return Builder.CreateFSub(ASTNumber(0).CodeGen(), Lhs->CodeGen(), "subtmp");
	}
	// Special case '=' because we don't want to emit the LHS as an expression.
	if (OpType == opASSIGN) 
	{
		// Assignment requires the LHS to be an identifier.
		// This assume we're building without RTTI because uilds that way by
		// default.  If you build ith RTTI this can be changed to a
		// dynamic_cast for automatic error checking.
		ASTVar * lhs = static_cast<ASTVar *>(Lhs);
		if (!lhs)
			return LogErrorV("destination of '=' must be a variable");
		// Codegen the RHS.
		Value * val = Rhs->CodeGen();
		if (!val)
			return NULL;

		// Look up the name.
		Value * var = NamedValues[lhs->Name];
		if (!var)
			return LogErrorV("Unknown variable name");

		Builder.CreateStore(val, var);
		return val;
	}

	Value * lhsVal = Lhs->CodeGen();
	Value * rhsVal = Rhs->CodeGen();

	if (!lhsVal || !rhsVal)
		return NULL;

	switch (OpType)
	{
		case opPLUS:
			return Builder.CreateFAdd(lhsVal, rhsVal, "addtmp");
		case opMINUS:
			return Builder.CreateFSub(lhsVal, rhsVal, "subtmp");
		case opTIMES:
			return Builder.CreateFMul(lhsVal, rhsVal, "multmp");
		case opDIVIDE:
			return Builder.CreateFDiv(lhsVal, rhsVal, "divtmp");
		case opMOD:
			return Builder.CreateFRem(lhsVal, rhsVal, "remtmp");
		case opEQ:
			lhsVal = Builder.CreateFCmpUEQ(lhsVal, rhsVal, "cmptmp");
			// Convert bool 0/1 to double 0.0 or 1.0
			return Builder.CreateUIToFP(lhsVal, Type::getDoubleTy(TheContext), "booltmp");
		case opNEQ:
			lhsVal = Builder.CreateFCmpUNE(lhsVal, rhsVal, "cmptmp");
			return Builder.CreateUIToFP(lhsVal, Type::getDoubleTy(TheContext), "booltmp");
		case opGT:
			lhsVal = Builder.CreateFCmpUGT(lhsVal, rhsVal, "cmptmp");
			return Builder.CreateUIToFP(lhsVal, Type::getDoubleTy(TheContext), "booltmp");
		case opLT:
			lhsVal = Builder.CreateFCmpULT(lhsVal, rhsVal, "cmptmp");
			return Builder.CreateUIToFP(lhsVal, Type::getDoubleTy(TheContext), "booltmp");
		case opGTE:
			lhsVal = Builder.CreateFCmpUGE(lhsVal, rhsVal, "cmptmp");
			return Builder.CreateUIToFP(lhsVal, Type::getDoubleTy(TheContext), "booltmp");
		case opLTE:
			lhsVal = Builder.CreateFCmpULE(lhsVal, rhsVal, "cmptmp");
			return Builder.CreateUIToFP(lhsVal, Type::getDoubleTy(TheContext), "booltmp");
		default:
			return LogErrorV("Invalid binary operator");	
	}
}

ASTCall::ASTCall(std::string callee, std::vector<ASTExpr *> args)
{
	Callee = callee;
	Args = std::move(args);
}

Value * ASTCall::CodeGen()
{
	// Look up the name in the global module table.
	Function * calleeF = GetFunction(Callee);
	if (!calleeF)
		return LogErrorV("Unknown function referenced");

	// If argument mismatch error.
	if (calleeF->arg_size() != Args.size())
		return LogErrorV("Incorrect # arguments passed");

	std::vector<Value *> argsV;
	for (unsigned i = 0, e = Args.size(); i != e; ++i) {
		argsV.push_back(Args[i]->CodeGen());
		if (!argsV.back())
			return NULL;
	}

	return Builder.CreateCall(calleeF, argsV, "calltmp");
}

ASTPrototype::ASTPrototype(std::string name, std::vector<std::string> params)
{
	Name = name;
	Params = std::move(params);
}

Function * ASTPrototype::CodeGen()
{
	// Make the function type:  double(double,double) etc.
	std::vector<Type *> doubles(Params.size(), Type::getDoubleTy(TheContext));
	FunctionType * funcType;

	if (Name == "main")
		funcType = FunctionType::get(Type::getInt32Ty(TheContext), doubles, false);
	else
		funcType = FunctionType::get(Type::getDoubleTy(TheContext), doubles, false);

	Function * func =
		Function::Create(funcType, Function::ExternalLinkage, Name, TheModule.get());

	// Set names for all arguments
	unsigned i = 0;
	for (auto & arg : func->args())
		arg.setName(Params[i++]);

	return func;
}

ASTFunction::ASTFunction(ASTPrototype * proto, ASTExpr * body, std::vector<ASTVar *> vars)
{
	Proto = proto;
	Body = body;
	Vars = vars;
}

Function * ASTFunction::CodeGen()
{
	// Transfer ownership of the prototype to the FunctionProtos map, but keep a
	// reference to it for use below.
	auto & p = *Proto;
	FunctionProtos[Proto->Name] = std::move(Proto);
	Function * theFunction = GetFunction(p.Name);
	if (!theFunction)
		return NULL;

	// If this is an operator, install it.
	// if (P.isBinaryOp())
	// 	BinopPrecedence[P.getOperatorName()] = P.getBinaryPrecedence();

	// Create a new basic block to start insertion into.
	BasicBlock * bb = BasicBlock::Create(TheContext, "entry", theFunction);
	Builder.SetInsertPoint(bb);

	// Record the function arguments in the NamedValues map.
	NamedValues.clear();
	for (auto & arg : theFunction->args()) 
	{
		// Create an alloca for this variable.
		AllocaInst *Alloca = CreateEntryBlockAlloca(theFunction, arg.getName());

		// Store the initial value into the alloca.
		Builder.CreateStore(&arg, Alloca);

		// Add arguments to variable symbol table.
		NamedValues[arg.getName()] = Alloca;
	}

	for (int i = 0; i < Vars.size(); i++) 
	{
		AllocaInst * alloca = CreateEntryBlockAlloca(theFunction, Vars[i]->Name);
		NamedValues[Vars[i]->Name] = alloca;
	}

	Value * retVal;
	if (Proto->Name == "main")
	{
		retVal = Body->CodeGen();
		if (retVal)
			retVal = Builder.CreateFPToSI(retVal, Type::getInt32Ty(TheContext), "inttmp");
	}
	else
		retVal = Body->CodeGen();

	if (retVal) 
	{
		// Finish off the function.
		Builder.CreateRet(retVal);

		// Validate the generated code, checking for consistency.
		verifyFunction(*theFunction);

		return theFunction;
	}

	// Error reading body, remove function.
	theFunction->eraseFromParent();
	return NULL;
}

ASTIf::ASTIf(ASTExpr * cond, ASTExpr * then, ASTExpr * els)
{
	Cond = cond;
	Then = then;
	Else = els;
}

Value * ASTIf::CodeGen()
{
	Value * condV = Cond->CodeGen();
	if (!condV)
		return NULL;

	// Convert condition to a bool by comparing non-equal to 0.0.
	condV = Builder.CreateFCmpONE(
	condV, ConstantFP::get(TheContext, APFloat(0.0)), "ifcond");

	Function * theFunction = Builder.GetInsertBlock()->getParent();

	// Create blocks for the then and else cases.  Insert the 'then' block at the
	// end of the function.
	BasicBlock * thenBB = BasicBlock::Create(TheContext, "then", theFunction);
	BasicBlock * elseBB = BasicBlock::Create(TheContext, "else");
	BasicBlock * mergeBB = BasicBlock::Create(TheContext, "ifcont");

	Builder.CreateCondBr(condV, thenBB, elseBB);

	// Emit then value.
	Builder.SetInsertPoint(thenBB);

	Value * thenV = Then->CodeGen();
	if (!thenV)
		return NULL;

	Builder.CreateBr(mergeBB);
	// Codegen of 'Then' can change the current block, update thenBB for the PHI.
	thenBB = Builder.GetInsertBlock();

	// Emit else block.
	theFunction->getBasicBlockList().push_back(elseBB);
	Builder.SetInsertPoint(elseBB);

	Value * elseV = Else->CodeGen();
	if (!elseV)
		return NULL;

	Builder.CreateBr(mergeBB);
	// Codegen of 'Else' can change the current block, update elseBB for the PHI.
	elseBB = Builder.GetInsertBlock();

	// Emit merge block.
	theFunction->getBasicBlockList().push_back(mergeBB);
	Builder.SetInsertPoint(mergeBB);
	PHINode * pn = Builder.CreatePHI(Type::getDoubleTy(TheContext), 2, "iftmp");

	pn->addIncoming(thenV, thenBB);
	pn->addIncoming(elseV, elseBB);
	return pn;
}

ASTProgram::ASTProgram(std::vector<ASTFunction *> glFuncs, std::vector<ASTVar *> glVars)
{
	GlFuncs = glFuncs;
	GlVars = glVars;
}

void ASTProgram::CodeGen()
{
	for (auto it : GlVars)
	{
		if (auto varIR = it->CodeGen())
		{
			varIR->print(errs());
			fprintf(stderr, "\n");
		}
	}

	for (auto it : GlFuncs)
	{
		if (auto fnIR = it->CodeGen())
		{
			fnIR->print(errs());
			fprintf(stderr, "\n");
		}
	}
}

void ASTNumber::Print()
{
	printf("%lf", Value);
}

void ASTVar::Print()
{
	printf("%s", Name.c_str());
}

void ASTBinOp::Print()
{
	printf("(");
	Lhs->Print();
	printf(")");
	printf(" %s ", ToString(OpType).c_str());
	if (Rhs)
	{
		printf("(");
		Rhs->Print();
		printf(")");
	}
}

void ASTCall::Print()
{
	printf("%s(", Callee.c_str());
	for (int i = 0; i < Args.size(); i++)
	{
		Args[i]->Print();
		if (i < Args.size() - 1)
			printf(", ");
	}
	printf(")");
}

void ASTIf::Print()
{
	printf("if (");
	Cond->Print();
	printf(")\nthen\n");
	Then->Print();
	printf("\nelse\n");
	Else->Print();
	printf("\nendif");
}

void ASTPrototype::Print()
{
	printf("def %s(", Name.c_str());
	for (int i = 0; i < Params.size(); i++)
	{
		printf("%s", Params[i].c_str());
		if (i < Params.size() - 1)
			printf(", ");
	}
	printf(")\n");
}

void ASTFunction::Print()
{
	Proto->Print();
	Body->Print();
	printf("\n");
}

void ASTProgram::Print()
{
	printf("\n=== AST Print ===\n");
	printf("global vars:\n");
	for (auto it : GlVars)
	{
		it->Print();
		printf("\n");
	}

	printf("functions:\n");
	for (auto it : GlFuncs)
		it->Print();
}

std::string ToString(BinOpType opType)
{
	switch (opType)
	{
		case opPLUS: return "+";
		case opMINUS: return "-";
		case opTIMES: return "*";
		case opDIVIDE: return "/";
		case opEQ: return "==";
		case opNEQ: return "!=";
		case opGT: return ">";
		case opLT: return "<";
		case opGTE: return ">=";
		case opLTE: return "<=";
		case opMOD: return "%";
		case opASSIGN: return "=";
	}
}