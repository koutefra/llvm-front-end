#include "lexan.hpp"

Token::Token(LexSymbol symbol, std::string value)
{
	Symbol = symbol;
	Value = value;
}

Lexan::Lexan()
{
	lineNumber = 1;
	nextChar = getchar();
	nextToken = getNextToken();
	lastToken = nextToken;
}

Token Lexan::GetNext()
{
	lastToken = nextToken;
	nextToken = getNextToken();
	return lastToken;
}

Token Lexan::GetNextWithoutShift() const
{
	return nextToken;
}

Token Lexan::GetLast() const
{
	return lastToken;
}

Token Lexan::getNextToken()
{
	LexSymbol symbol;

	while (isspace(nextChar)) // skip white spaces
	{
		if (nextChar == '\n')
			lineNumber++;
		nextChar = getchar();
	}

	if (isalpha(nextChar)) // [a-Z] (identifier or keyword)
	{
		std::string value = "";
		do
		{
			value += nextChar;
			nextChar = getchar();
		} while (isalpha(nextChar) || isdigit(nextChar));

		return Token(findKeyword(value), value);
	}
	else if (isdigit(nextChar)) // number
	{
		std::string value = "";
		do
		{
			value += nextChar;
			nextChar = getchar();
		} while (isdigit(nextChar));

		return Token(NUMBER, value);		
	}
	else if ((symbol = parseOperator()) != ERR) // operator
	{
		return Token(symbol, "");
	}
	else if (nextChar == '#') // comment
	{
		while ((nextChar = getchar()) != '\n' && nextChar != EOF);
		if (nextChar == EOF) // EOF after comment, otherwise newline (end of comment)
			return Token(EOI, "");
		lineNumber++;
		return getNextToken();
	}
	else if (nextChar == EOF)
	{
		return Token(EOI, "");
	}

	throw "Invalid character '" + std::string(1, nextChar) + "'" + "(line " 
		+ std::to_string(lineNumber) + ")\n";	
}

LexSymbol Lexan::findKeyword(std::string value) const
{
	auto it = KeywordTable.find(value);
	if (it == KeywordTable.end())
		return IDENT;

	return it->second;
}

LexSymbol Lexan::parseOperator()
{
	switch (nextChar)
	{
		case '+':
			nextChar = getchar();
			return PLUS;
		case '-':
			nextChar = getchar();
			return MINUS;
		case '*':
			nextChar = getchar();
			return TIMES;
		case '/':
			nextChar = getchar();
			return DIVIDE;
		case '(':
			nextChar = getchar();
			return LPAR;
		case ')':
			nextChar = getchar();
			return RPAR;
		case ',':
			nextChar = getchar();
			return COMMA;
		case '=':
			nextChar = getchar();
			if (nextChar == '=')
			{
				nextChar = getchar();
				return EQ;
			}
			return ASSIGN;
		case '<':
			nextChar = getchar();
			switch (nextChar)
			{
				case '=':
					nextChar = getchar();
					return LTE;
				case '>':
					nextChar = getchar();
					return NEQ;
			}
			return LT;
		case '>':
			nextChar = getchar();
			if (nextChar == '=')
			{
				nextChar = getchar();
				return GTE;				
			}
			return GT;
		case '%':
			nextChar = getchar();
			return MOD;
	}

	return ERR;
}

bool Lexan::IsNext(LexSymbol symbol) const
{
	return symbol == nextToken.Symbol;
}

Token Lexan::Match(LexSymbol symbol)
{
	if (!IsNext(symbol))
		throw "Unexpected token (line " + std::to_string(lineNumber) + ")\n";

	return GetNext();
}

bool Lexan::IsNextOperator() const
{
	if (nextToken.IsOperator())
		return true;

	return false;
}

bool Token::IsOperator() const
{
	if (Symbol == PLUS || Symbol == PLUS || Symbol == MINUS || Symbol == TIMES || 
		Symbol == DIVIDE || Symbol == EQ || Symbol == NEQ || Symbol == GT ||
		Symbol == GTE || Symbol == LT || Symbol == LTE || Symbol == MOD || 
		Symbol == ASSIGN)
		return true;

	return false;	
}